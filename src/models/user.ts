import * as moment from 'moment';
import { Billing } from "./billing";
import { Plan } from "./plan";

export class User {
    public id: string;
    public firstName: string = "";
    public middleName: string = "";
    public lastName: string = "";
    public emailAddress: string = "";
    public zipCode: number;
    public cityCountry: string = "";
    public dayRate: number;
    public mobileNumber: string = "";
    public instagram: string = "";
    public youtube: string = "";
    public position: string = "";
    public picture: string = "";
    public plan: Plan;
    public billing: Billing;
    public creationTime: Date | string = moment().format();

    constructor(init?: Partial<User>) {
        Object.assign(this, init);
    }

    get name() {
        return `${this.firstName} ${this.lastName}`;
    }

    get location() {
        if (!this.cityCountry && !this.zipCode) {
            return "";
        }
        return `${this.cityCountry} ${this.zipCode}`;
    }
}