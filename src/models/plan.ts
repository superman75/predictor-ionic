export class Plan {
    public days: number;
    public membership: string = "";

    constructor(init?: Partial<Plan>) {
        Object.assign(this, init);
    }
}