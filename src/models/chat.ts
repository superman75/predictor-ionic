import * as moment from 'moment';

export class Chat {
    public message: string;
    public pair: string;
    public sender: string;
    public time: Date | string = moment().format();

    constructor(init?: Partial<Chat>) {
        Object.assign(this, init);
    }

    get readableTime() {
        return moment(this.time).fromNow();
    }
}