import * as moment from 'moment';

export class Gig {
    public id: string;
    public creator: string = "";
    public position: string = "";
    public startDate: Date | string = "";
    public endDate: Date | string = "";
    public rate: number;
    public company: string = "";
    public location: string = "";
    public emailAddress: string = "";

    constructor(init?: Partial<Gig>) {
        Object.assign(this, init);
    }

    get date() {
        const start = moment(this.startDate).format("MM.DD.YYYY");
        const end = moment(this.endDate).format("MM.DD.YYYY");
        if (!this.startDate || !this.endDate) {
            return "";
        }
        return `${start} - ${end}`;
    }
}

export const GigPositions = [
    "DP/Videographer",
    "Photographer",
    "Producer",
    "Assistant Director",
    "Production Assistant",
    "Editor",
    "Sound",
    "Graphic Designer",
    "Hair and Makeup"
];