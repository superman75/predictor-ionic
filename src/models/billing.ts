export class Billing {
    public nameOnCard: string = "";
    public cardNumber: string = "";
    public expiryDate: string = "";
    public cvv: number;
    public address: number;
    public zipCode: string = "";
    public cityCountry: number;

    constructor(init?: Partial<Billing>) {
        Object.assign(this, init);
    }
}