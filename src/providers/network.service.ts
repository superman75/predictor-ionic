import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthProvider } from './auth.service';
import { User } from '../models/user';

@Injectable()
export class NetworkService {

  private usersLocation: string = "users";

  constructor(private fireStore: AngularFirestore,
    private auth: AuthProvider) {
  }

  public getNetworks(position) {
    return new Promise(async (resolve, reject) => {
      try {
          const user: any = await this.auth.getLoggedInUser();
          const collectionRef = await this.fireStore.collection(this.usersLocation);
          const snapshot = await collectionRef.ref.get();
          const allPreditorsExceptLoggedInUser = snapshot.docs.filter(
            d => d.id != user.email &&
            d.data().position.toLocaleLowerCase() === position.toLowerCase()
          );
          const networks = allPreditorsExceptLoggedInUser.map(doc => new User({ ...doc.data(), id: doc.id }));
          resolve(networks);
      } catch (e) {
          console.log("Get preditors failed", e);
          reject(e.message);
      }
  });
  }

}