import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GooglePlus } from '@ionic-native/google-plus';

import { User } from '../models/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { ProfileProvider } from './profile.service';
import { UtilitiesProvider } from './utilities';
import { Facebook } from '@ionic-native/facebook';

@Injectable()
export class AuthProvider {

  constructor(public http: HttpClient,
    private fireAuth: AngularFireAuth,
    private profileProvider: ProfileProvider,
    private utilities: UtilitiesProvider,
    private googlePlus: GooglePlus,
    private facebook: Facebook) {
    console.log('Hello AuthProvider Provider');
  }

  register(user: User, password, resume, picture) {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.fireAuth.auth.createUserWithEmailAndPassword(user.emailAddress, password);

        // set the name of newly created user to the jwt
        response.user.updateProfile({
          displayName: user.name,
          photoURL: ""
        });

        // add the profile info to the database
        await this.profileProvider.updateProfile(user);

        // add the resume to the storage
        if (resume) {
          await this.profileProvider.updateResume(resume);
        }

        // add the picture to the storage
        if (picture) {
          await this.profileProvider.updatePicture(picture);
        }

        console.log("Registration successful");
        // resolve the response
        resolve(response.user);
      } catch (e) {
        console.log("Registration failed", e);
        reject(e.message);
      }
    });
  }

  login(email, password) {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.fireAuth.auth.signInWithEmailAndPassword(email, password);
        console.log("Login successful");
        resolve(response.user);
      } catch (e) {
        console.log("Login failed", e);
        reject(e.message);
      }
    });
  }

  loginViaGoogle() {
    return new Promise(async (resolve, reject) => {
      try {
        let response: any;
        // check if from browser or cordova
        if (this.utilities.isApp()) {
          const gPlusResponse = await this.googlePlus.login({
            "webClientId": "835863859224-fg0he9gpai3nkus5mflffrh347mjlt1i.apps.googleusercontent.com",
            "offline": true,
            "scopes": "profile email"
          });
          response = await this.fireAuth.auth.signInWithCredential(auth.GoogleAuthProvider.credential(gPlusResponse.idToken));
        } else {
          response = await this.fireAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
        }
        // check if has profile, if none create it
        try {
          await this.profileProvider.getProfile();
        } catch (e) {
          if (e == "No profile found") {
            const user = this.createUserProfileFromProvider(response.additionalUserInfo.profile);
            await this.profileProvider.updateProfile(user);
          }
        }
        console.log("Login successful");
        resolve(response.user);
      } catch (e) {
        console.log("Login failed", e);
        reject(e.message);
      }
    });
  }

  loginViaFacebook() {
    return new Promise(async (resolve, reject) => {
      try {
        let response: any;
        // check if from browser or cordova
        if (this.utilities.isApp()) {
          console.log('test1');
          const facebookResponse = await this.facebook.login(["public_profile", "email", "user_friends"]);
          response = await this.fireAuth.auth.signInWithCredential(auth.FacebookAuthProvider.credential(facebookResponse.authResponse.accessToken));
        } else {
          console.log('test2');
          response = await this.fireAuth.auth.signInWithPopup(new auth.FacebookAuthProvider());
        }
        // check if has profile, if none create it
        try {
          await this.profileProvider.getProfile();
        } catch (e) {
          if (e == "No profile found") {
            const userDetailFromFacebook = await this.facebook.api(`/${response.providerData[0].uid}/?fields=id,email,first_name,last_name,picture,address`, ["public_profile"]);
            const user = this.createUserProfileFromProvider(userDetailFromFacebook);
            await this.profileProvider.updateProfile(user);
          }
        }
        console.log("Login successful");
        resolve(response.user);
      } catch (e) {
        console.log("Login failed", e);
        reject(e.message);
      }
    });
  }

  createUserProfileFromProvider(providerData) {
    let picture = providerData.picture || providerData.photoURL;
    // if picture is an object, this came from facebook then
    // retrieve the inner values
    if(typeof picture === "object" && picture !== null){
      picture = picture.data.url;
    }

    return new User({
      firstName: providerData.given_name || providerData.first_name,
      lastName: providerData.family_name || providerData.last_name,
      emailAddress: providerData.email,
      mobileNumber: providerData.phoneNumber || "",
      picture
    });
  }

  logout() {
    return new Promise(async (resolve, reject) => {
      try {
        await this.fireAuth.auth.signOut();
        console.log("Logout successful");
        resolve("Logout successful");
      } catch (e) {
        console.log("Logout failed", e);
        reject(e.message);
      }
    });
  }

  getLoggedInUser() {
    return new Promise((resolve, reject) => {
      this.fireAuth.auth.onAuthStateChanged(async user => {
        if (user) {
          resolve(user);
        } else {
          reject(null);
        }
      });
    });
  }

  isLoggedIn = this.getLoggedInUser;

}
