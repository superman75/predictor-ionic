import * as moment from 'moment';
import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Chat } from '../models/chat';
import { AuthProvider } from './auth.service';

@Injectable()
export class ChatProvider {

  chats: AngularFirestoreCollection<any>;
  private chatsLocation: string = "chats";

  constructor(private fireStore: AngularFirestore,
    private auth: AuthProvider) {
    console.log('Hello ChatProvider Provider');
    this.chats = this.fireStore.collection(this.chatsLocation);
  }

  async addChat(message, otherUser) {
    const currentUser: any = await this.auth.getLoggedInUser();
    const chat = new Chat({
      message,
      pair: await this.createPairId(otherUser),
      sender: currentUser.email,
    });
    return await this.chats.add({ ...chat });
  }

  async getChats(otherUser) {
    const pairId = await this.createPairId(otherUser);
    return this.fireStore.collection<Chat>(this.chatsLocation, res => {
      return res.where("pair", "==", pairId);
    });
  }

  async getLastChat(otherUser) {
    const pairId = await this.createPairId(otherUser);
    return this.fireStore.collection<Chat>(this.chatsLocation, res => {
      return res.where("pair", "==", pairId)
              .orderBy("time", "desc")
              .limit(1);
    });
  }

  async createPairId(otherUser) {
    const currentUser: any = await this.auth.getLoggedInUser();
    if (moment(currentUser.metadata.creationTime) < moment(otherUser.creationTime)) {
      return `${currentUser.email}|${otherUser.emailAddress}`;
    } else {
      return `${otherUser.emailAddress}|${currentUser.email}`;
    }
  }

}
