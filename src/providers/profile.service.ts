import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { Storage } from '@ionic/storage'

@Injectable()
export class ProfileProvider {

  private user: any = {};
  private usersLocation: string = "users";
  private resumesLocation: string = "resumes";
  private picturesLocation: string = "pictures";

  constructor(private fireStore: AngularFirestore,
    private fireStorage: AngularFireStorage,
    private fireAuth: AngularFireAuth,
    private storage: Storage) {
    console.log('Hello ProfileProvider Provider');
  }

  init() {
    return new Promise((resolve) => {
      this.fireAuth.auth.onAuthStateChanged(async user => {
        console.log("changing auth state", user);
        if (user) {
          this.user = user;
        } else {
          this.user = {};
        }
        resolve();
      });
    });
  }

  getProfile() {
    return new Promise(async (resolve, reject) => {
      try {
        const doc = await this.fireStore.collection(this.usersLocation).doc(this.user.email).get().toPromise();
        if (doc.exists) {
          const profile = new User({ ...doc.data() });
          return resolve(profile);
        } else {
          return reject("No profile found");
        }
      } catch (e) {
        console.log("Get profile failed", e);
        reject(e.message);
      }
    });
  }

  getProfilePicture() {
    return new Promise(async (resolve, reject) => {
      try {
        // check if there is an image in the local storage
        const image = await this.storage.get("picture");
        if (image) {
          return resolve(image);
        }
        // check if there is an image in the profile
        const profile: any = await this.getProfile();
        if (profile.picture) {
          return resolve(profile.picture);
        }
        // get from firestorage if all else fails
        const url = await this.fireStorage.ref(`${this.picturesLocation}/${this.user.email}`).getDownloadURL().toPromise();
        return resolve(url);
      } catch (e) {
        console.log("Get profile picture failed", e);
        reject(e.message);
      }
    });
  }

  updateProfile(user) {
    return new Promise(async (resolve, reject) => {
      try {
        await this.fireStore.collection(this.usersLocation).doc(this.user.email).set({ ...user }, { merge: true });
        console.log("Update profile successful");
        resolve("Update profile successful");
      } catch (e) {
        console.log("Update profile failed", e);
        reject(e.message);
      }
    });
  }

  updateResume(resume) {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.fireStorage.upload(`${this.resumesLocation}/${this.user.email}`, resume);
        console.log("Update resume successful");
        resolve(response);
      } catch (e) {
        console.log("Update resume failed", e);
        reject(e.message);
      }
    });
  }

  updatePicture(picture) {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.fireStorage.ref(`${this.picturesLocation}/${this.user.email}`).put(picture);
        const url = await this.fireStorage.ref(`${this.picturesLocation}/${this.user.email}`).getDownloadURL().toPromise();
        await this.updateProfile({ picture: url });
        await this.storePictureInLocalStorage(picture);
        console.log("Update picture successful");
        resolve(response);
      } catch (e) {
        console.log("Update picture failed", e);
        reject(e.message);
      }
    });
  }

  updatePosition(position) {
    return new Promise(async (resolve, reject) => {
      try {
        this.fireStore.collection(`users`).doc(this.user.email).update({
          position
        });
        console.log("Update position successful");
        resolve("Update position successful");
      } catch (e) {
        console.log("Update position failed", e);
        reject(e.message);
      }
    });
  }

  updatePlan(plan) {
    return new Promise(async (resolve, reject) => {
      try {
        this.fireStore.collection(`users`).doc(this.user.email).update({
          plan: {
            ...plan
          }
        });
        console.log("Update plan successful");
        resolve("Update plan successful");
      } catch (e) {
        console.log("Update plan failed", e);
        reject(e.message);
      }
    });
  }

  updateBilling(billing) {
    return new Promise(async (resolve, reject) => {
      try {
        this.fireStore.collection(`users`).doc(this.user.email).update({
          billing: {
            ...billing
          }
        });
        console.log("Update billing successful");
        resolve("Update billing successful");
      } catch (e) {
        console.log("Update billing failed", e);
        reject(e.message);
      }
    });
  }

  // Returns the screen that should be shown to the user upon opening the app
  // Use this after each call of submit button in the registration flow
  getStartupPage() {
    return new Promise(async (resolve) => {
      try {
        const profile: any = await this.getProfile();
        console.log('profile', profile);
        if (profile.position == null) {
          return resolve("WhatYouDoPage");
        }

        if (profile.plan == null || profile.billing == null) {
          return resolve("PlanAndBillingPage");
        }

        return resolve("TabsPage");
      } catch (e) {
        console.log("GetStartupPage failed", e);
        resolve(null);
      }
    });
  }

  storePictureInLocalStorage(picture) {
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.onload = async (e: any) => {
        this.storage.set("picture", e.target.result);
        console.log("Picture stored in the localstorage");
        resolve();
      };
      reader.onerror = resolve;
      reader.readAsDataURL(picture);
    });
  }
}
