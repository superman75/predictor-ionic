import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthProvider } from './auth.service';
import { Gig } from '../models/gig';
import { AngularFireStorage } from '@angular/fire/storage';
import * as firebase from 'firebase/app';

@Injectable()
export class GigService {

    private gigsLocation: string = "gigs";
    private picturesLocation: string = "gigpics";

    constructor(private http: HttpClient,
        private fireStore: AngularFirestore,
        private fireStorage: AngularFireStorage,
        private auth: AuthProvider) {
    }

    getGig(id) {
        return new Promise(async (resolve, reject) => {
            try {
                const doc = await this.fireStore.collection(this.gigsLocation).doc(id).get().toPromise();
                if (doc.exists) {
                    const gig = new Gig({ ...doc.data() });
                    resolve(gig);
                } else {
                    reject("No gig found");
                }
            } catch (e) {
                console.log("Get gig failed", e);
                reject(e.message);
            }
        });
    }

    getAvailableGigs() {
        return new Promise(async (resolve, reject) => {
            try {
                const collectionRef = await this.fireStore.collection(this.gigsLocation);
                const today = new Date();
                today.setHours(0, 0, 0, 0);

                const snapshot = await collectionRef.ref.where("startDate", ">=", today.getTime()).get();
                const availableGigs = snapshot.docs.map(doc => new Gig({ ...doc.data(), id: doc.id }));
                resolve(availableGigs);
            } catch (e) {
                console.log("Get available gigs failed", e);
                reject(e.message);
            }
        });
    }

    getMyGigs(userParam?:any) {
        return new Promise(async (resolve, reject) => {
            try {
                const user: any = userParam || await this.auth.getLoggedInUser();
                const email = user.email || user.emailAddress;
                const collectionRef = await this.fireStore.collection(this.gigsLocation);
                const snapshot = await collectionRef.ref
                    .where("creator", "==", email)
                    .get();
                const availableGigs = snapshot.docs.map(doc => new Gig({ ...doc.data(), id: doc.id }));
                resolve(availableGigs);
            } catch (e) {
                console.log("Get my avaialble gigs failed", e);
                reject(e.message);
            }
        });
    }

    getMyAppliedGigs(userParam?:any) {
        return new Promise(async (resolve, reject) => {
            try {
                const user: any = userParam || await this.auth.getLoggedInUser();
                const email = user.email || user.emailAddress;
                const collectionRef = await this.fireStore.collection(this.gigsLocation);
                const snapshot = await collectionRef.ref
                    .where("talents", "array-contains", email)
                    .get();
                const availableGigs = snapshot.docs.map(doc => new Gig({ ...doc.data(), id: doc.id }));
                resolve(availableGigs);
            } catch (e) {
                console.log("Get my avaialble gigs failed", e);
                reject(e.message);
            }
        });
    }

    addGig(gig: Gig) {
        return new Promise(async (resolve, reject) => {
            try {
                const user: any = await this.auth.getLoggedInUser();
                const docRef = await this.fireStore.collection(this.gigsLocation).add({ ...gig, creator: user.email });
                console.log("Add gig successful");
                resolve(docRef);
            } catch (e) {
                console.log("Add gig failed", e);
                reject(e.message);
            }
        });
    }

    updateGigPicture(id, picture) {
        return new Promise(async (resolve, reject) => {
            try {
                const response = await this.fireStorage.upload(`${this.picturesLocation}/${id}`, picture);
                console.log("Update picture successful");
                resolve(response);
            } catch (e) {
                console.log("Update picture failed", e);
                reject(e.message);
            }
        });
    }

    getGigPicture(id) {
        return new Promise(async (resolve, reject) => {
            try {
                const url = await this.fireStorage.ref(`${this.picturesLocation}/${id}`).getDownloadURL().toPromise();
                resolve(url);
            } catch (e) {
                console.log("Get picture failed", e);
                reject(e.message);
            }
        });
    }

    applyToGig(id) {
        return new Promise(async (resolve, reject) => {
            try {
                const user: any = await this.auth.getLoggedInUser();
                await this.fireStore.collection(this.gigsLocation).doc(id)
                    .update({
                        talents: firebase.firestore.FieldValue.arrayUnion(user.email)
                    });
                console.log("Apply to gig successful");
                resolve("Apply to gig successful");
            } catch (e) {
                console.log("Apply to gig failed", e);
                reject(e.message);
            }
        });
    }

}