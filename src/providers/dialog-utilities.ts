import { Injectable } from '@angular/core';
import { ToastController, LoadingController, Loading } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Injectable()
export class DialogUtilitiesProvider {

  loader: Loading;

  constructor(private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private iap: InAppBrowser) {
    console.log('Hello DialogUtilitiesProvider Provider');
  }

  showLoading(message = "") {
    this.loader = this.loadingCtrl.create({
      content: message
    });
    this.loader.present();
  }

  hideLoading() {
    if (this.loader != null) {
      this.loader.dismiss();
    }
  }

  showToast(message, duration = 3000) {
    this.toastCtrl.create({
      message: message,
      duration: duration,
      showCloseButton: true
    }).present();
  }

  openInAppBrowser(url) {
    this.iap.create(url).show();
  }

}
