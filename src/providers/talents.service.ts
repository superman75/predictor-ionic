import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TalentService {

    constructor(private http: HttpClient) {
    }

    getTalents(): Observable<any> {
        return this.http.get("assets/data/talents.json")
    }

}