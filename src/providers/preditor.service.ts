import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AuthProvider } from './auth.service';
import { User } from '../models/user';

@Injectable()
export class PreditorService {

    private usersLocation: string = "users";

    constructor(private fireStore: AngularFirestore,
        private fireStorage: AngularFireStorage,
        private auth: AuthProvider) {
    }

    getPreditors() {
        return new Promise(async (resolve, reject) => {
            try {
                const user: any = await this.auth.getLoggedInUser();
                const collectionRef = await this.fireStore.collection(this.usersLocation);
                const snapshot = await collectionRef.ref.get();
                const allPreditorsExceptLoggedInUser = snapshot.docs.filter(d => d.id != user.email);
                const allPreditors = allPreditorsExceptLoggedInUser.map(doc => new User({ ...doc.data(), id: doc.id }));
                resolve(allPreditors);
            } catch (e) {
                console.log("Get preditors failed", e);
                reject(e.message);
            }
        });
    }

    getPreditorByEmail(email: string) {
        return new Promise(async (resolve, reject) => {
            try {
                const collectionRef = await this.fireStore.collection(this.usersLocation);
                const snapshot = await collectionRef.ref.where("emailAddress", "==", email).get();
                if (snapshot.docs.length > 0) {
                    const preditors = snapshot.docs.map(doc => new User({ ...doc.data(), id: doc.id }));
                    resolve(preditors[0]);
                } else {
                    reject('Preditor not found');
                }                
            } catch (e) {
                reject(e.message);
            }
        });
    }

}