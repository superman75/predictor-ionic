import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { User } from '../../models/user';
import { AuthProvider } from '../../providers/auth.service';
import { ProfileProvider } from '../../providers/profile.service';
import { DialogUtilitiesProvider } from '../../providers/dialog-utilities';
import { CalendarComponentOptions } from 'ion2-calendar';
import { GigService } from '../../providers/gigs.service';
import { Gig } from '../../models/gig';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [GigService]
})
export class ProfilePage {

  user: User;
  @ViewChild("imageFile") imageFile: any;
  image: string;
  imageFileHolder: any;
  calendarOptions: CalendarComponentOptions;
  isEditing: boolean = false;

  constructor(private navCtrl: NavController,
    private auth: AuthProvider,
    private profileProvider: ProfileProvider,
    private dialogUtilities: DialogUtilitiesProvider,
    private gigService: GigService) {

  }

  // Add this to all the pages that needs checking if logged in
  async ionViewCanEnter() {
    return await this.auth.isLoggedIn();
  }

  async ionViewWillEnter() {
    this.writeBusyDates();
    this.user = await this.getUserInfo();
    this.image = this.user.picture || null;
    console.log(this.user);
  }

  async getUserInfo() {
    try {
      this.profileProvider.getProfilePicture().then((url: any) => this.image = url);
      const profile: any = await this.profileProvider.getProfile();
      return profile;
    } catch (e) {
      this.dialogUtilities.showToast("There was an issue retrieving billing info");
    }
    return null;
  }

  async writeBusyDates() {
    const appliedGigs = await this.writeMyAppliedGigsDate();
    const myGigs = await this.writeMyGigsDate();
    const busyDates = appliedGigs.concat(myGigs);
    this.calendarOptions = {
      daysConfig: busyDates
    };
  }

  async writeMyAppliedGigsDate() {
    let daysConfig = [];
    try {
      const appliedGigs: Gig[] = await this.gigService.getMyAppliedGigs() as Gig[];
      for (let gig of appliedGigs) {
        // make a range from startDate to endDate
        let currentDate = moment(gig.startDate);
        let stopDate = moment(gig.endDate);
        while (currentDate <= stopDate) {
          daysConfig.push({
            marked: true,
            date: currentDate.toDate(),
            subtitle: gig.company,
            cssClass: "have-gig"
          });
          currentDate = moment(currentDate).add(1, "days");
        }
      }
    } catch (e) {
      console.log(e);
    }
    return Promise.resolve(daysConfig);
  }

  async writeMyGigsDate() {
    let daysConfig = [];
    try {
      const appliedGigs: Gig[] = await this.gigService.getMyGigs() as Gig[];
      for (let gig of appliedGigs) {
        // make a range from startDate to endDate
        let currentDate = moment(gig.startDate);
        let stopDate = moment(gig.endDate);
        while (currentDate <= stopDate) {
          daysConfig.push({
            marked: true,
            date: currentDate.toDate(),
            subtitle: gig.company,
            cssClass: "my-gig"
          });
          currentDate = moment(currentDate).add(1, "days");
        }
      }
    } catch (e) {
      console.log(e);
    }
    return Promise.resolve(daysConfig);
  }

  pickImage() {
    if (!this.isEditing) {
      return;
    }
    this.imageFile.nativeElement.click();
  }

  changeImage(files) {
    const reader = new FileReader();
    reader.onload = async (e: any) => {
      this.image = e.target.result;
    };
    if (files.length > 0) {
      reader.readAsDataURL(files[0]);
      this.imageFileHolder = files[0];
    }
  }

  openBrowser(url) {
    this.dialogUtilities.openInAppBrowser(url);
  }

  edit() {
    this.isEditing = !this.isEditing;
    // remove image file holder
    this.imageFileHolder = "";
  }

  update() {
    if(this.imageFileHolder){
      this.profileProvider.updatePicture(this.imageFileHolder);
    }
    this.profileProvider.updateProfile(this.user);
    this.isEditing = false;
  }
}