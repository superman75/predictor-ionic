import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalendarModule } from "ion2-calendar";
import { ProfilePage } from './profile';

@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePage),
    CalendarModule
  ],
  exports: [
    ProfilePage
  ]
})
export class ProfilePageModule { }
