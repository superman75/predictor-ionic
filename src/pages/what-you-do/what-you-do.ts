import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { DialogUtilitiesProvider } from '../../providers/dialog-utilities';
import { ProfileProvider } from '../../providers/profile.service';
import { AuthProvider } from '../../providers/auth.service';
import { GigPositions } from '../../models/gig';

@IonicPage()
@Component({
  selector: 'page-what-you-do',
  templateUrl: 'what-you-do.html'
})
export class WhatYouDoPage {

  items: any = GigPositions;
  selectedItem: any = this.items[0];

  constructor(private navCtrl: NavController,
    private profileProvider: ProfileProvider,
    private dialogUtilities: DialogUtilitiesProvider,
    private auth: AuthProvider) {

  }

  // Add this to all the pages that needs checking if logged in
  async ionViewCanEnter() {
    return await this.auth.isLoggedIn();
  }

  itemSelected(item) {
    this.selectedItem = item;
  }

  async goToDaysPlan() {
    try {
      await this.profileProvider.updatePosition(this.selectedItem);
      const nextPage: any = await this.profileProvider.getStartupPage();
      this.navCtrl.push(nextPage);
    } catch (e) {
      this.dialogUtilities.showToast(e);
    }
  }
}