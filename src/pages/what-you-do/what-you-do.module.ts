import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { WhatYouDoPage } from './what-you-do';

@NgModule({
  declarations: [
    WhatYouDoPage,
  ],
  imports: [
    IonicPageModule.forChild(WhatYouDoPage)
  ],
  exports: [
    WhatYouDoPage
  ]
})
export class WhatYouDoPageModule { }
