import { Component, ViewChild } from '@angular/core';
import { IonicPage, Slides } from 'ionic-angular';
import { PreditorService } from '../../providers/preditor.service';
import { DialogUtilitiesProvider } from '../../providers/dialog-utilities';
import { GigPositions } from '../../models/gig';

@IonicPage()
@Component({
  selector: 'page-user-search',
  templateUrl: 'user-search.html',
  providers: [PreditorService]
})
export class UserSearchPage {

  allPreditors: any = [];
  preditors: any = [];
  positions: any[] = GigPositions;
  selectedItem: any = "";
  searchKey: string = "";
  @ViewChild(Slides) slides: Slides;

  constructor(private preditorService: PreditorService,
    private dialogUtilities: DialogUtilitiesProvider) {

  }

  async ionViewWillEnter() {
    await this.resetPreditors();
    this.preditors = this.allPreditors;
  }

  async resetPreditors() {
    try {
      this.allPreditors = await this.preditorService.getPreditors();
    } catch (e) {
      this.dialogUtilities.showToast(e);
      console.log("Error while retrieving preditors", e);
    }
  }

  selectItem(item) {
    if (this.selectedItem == item) {
      // unselect
      this.selectedItem = "";
    } else {
      // select
      this.selectedItem = item;
    }
    this.searchPreditors();
  }

  searchPreditors() {
    // Do the search
    this.preditors = this.allPreditors.filter(preditor => {
      const nameMatched = preditor.name.toLowerCase().indexOf(this.searchKey) != -1;
      const positionMatched = preditor.position.toLowerCase() == this.selectedItem.toLowerCase();
      const noPositionSelected = this.selectedItem === "";

      return nameMatched && (positionMatched || noPositionSelected);
    });
  }
}