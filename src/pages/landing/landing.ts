import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html'
})
export class LandingPage {

    constructor(private navCtrl: NavController) {

    }

    goToSignIn() {
        this.navCtrl.setRoot('SignInPage');
    }

}