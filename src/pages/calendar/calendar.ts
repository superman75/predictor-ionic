import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { CalendarComponentOptions } from 'ion2-calendar';
import { GigService } from '../../providers/gigs.service';
import { Gig } from '../../models/gig';
import * as moment from 'moment';
import { DialogUtilitiesProvider } from '../../providers/dialog-utilities';

@IonicPage()
@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
  providers: [GigService]
})
export class CalendarPage {

  calendarOptions: CalendarComponentOptions;
  user: any;

  constructor(private navParams: NavParams,
    private gigService: GigService,
    private dialogUtilities: DialogUtilitiesProvider) {

  }

  async ionViewWillEnter() {
    this.user = this.navParams.get("user");
    try {
      this.dialogUtilities.showLoading();
      await this.writeBusyDates();
    } catch (e) {
      console.log("Error writing busy dates", e);
    } finally {
      this.dialogUtilities.hideLoading();
    }
  }

  async writeBusyDates() {
    const appliedGigs = await this.writeMyAppliedGigsDate();
    const myGigs = await this.writeMyGigsDate();
    const busyDates = appliedGigs.concat(myGigs);
    this.calendarOptions = {
      daysConfig: busyDates
    };
  }

  async writeMyAppliedGigsDate() {
    let daysConfig = [];
    try {
      const appliedGigs: Gig[] = await this.gigService.getMyAppliedGigs(this.user) as Gig[];
      for (let gig of appliedGigs) {
        // make a range from startDate to endDate
        let currentDate = moment(gig.startDate);
        let stopDate = moment(gig.endDate);
        while (currentDate <= stopDate) {
          daysConfig.push({
            marked: true,
            date: currentDate.toDate(),
            subtitle: gig.company,
            cssClass: "have-gig"
          });
          currentDate = moment(currentDate).add(1, "days");
        }
      }
    } catch (e) {
      console.log(e);
    }
    return Promise.resolve(daysConfig);
  }

  async writeMyGigsDate() {
    let daysConfig = [];
    try {
      const appliedGigs: Gig[] = await this.gigService.getMyGigs(this.user) as Gig[];
      for (let gig of appliedGigs) {
        // make a range from startDate to endDate
        let currentDate = moment(gig.startDate);
        let stopDate = moment(gig.endDate);
        while (currentDate <= stopDate) {
          daysConfig.push({
            marked: true,
            date: currentDate.toDate(),
            subtitle: gig.company,
            cssClass: "my-gig"
          });
          currentDate = moment(currentDate).add(1, "days");
        }
      }
    } catch (e) {
      console.log(e);
    }
    return Promise.resolve(daysConfig);
  }

}