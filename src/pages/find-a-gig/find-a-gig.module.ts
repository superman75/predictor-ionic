import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FindAGigPage } from './find-a-gig';

@NgModule({
  declarations: [
    FindAGigPage
  ],
  imports: [
    IonicPageModule.forChild(FindAGigPage)
  ],
  exports: [
    FindAGigPage
  ]
})
export class FindAGigPageModule { }
