import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides, Platform } from 'ionic-angular';
import { GigService } from '../../providers/gigs.service';
import { PreditorService } from '../../providers/preditor.service';
import { DialogUtilitiesProvider } from '../../providers/dialog-utilities';

@IonicPage()
@Component({
  selector: 'page-find-a-gig',
  templateUrl: 'find-a-gig.html',
  providers: [GigService, PreditorService]
})
export class FindAGigPage {

  @ViewChild(Slides) slides: Slides;
  canLeave: Boolean = true;
  gigs: any = [];
  selectedGig: any = {};
  gigPicture: any = null;

  constructor(private platform: Platform,
    private gigService: GigService,
    private dialogUtilities: DialogUtilitiesProvider,
    private preditorService: PreditorService,
    private navCtrl: NavController) {
    this.platform.backButton.subscribe(data => {
      if (this.slides.getActiveIndex() == 1) {
        this.canLeave = false
        this.slides.slidePrev();
        setTimeout(() => {
          this.canLeave = true;
        }, 300);
      }
    });
  }

  async ionViewWillEnter() {
    this.slides.enableKeyboardControl(false);
    this.slides.lockSwipeToNext(true);
    this.gigs = await this.getAvailableGigs();
  }

  ionViewCanLeave() {
    return this.canLeave;
  }

  async getAvailableGigs() {
    try {
      const gigs: any = await this.gigService.getAvailableGigs();
      console.log('gigs', gigs);
      return gigs;
    } catch (e) {
      this.dialogUtilities.showToast("There was an issue retrieving gigs");
    }
    return [];
  }

  async selectItem(gig) {
    this.selectedGig = gig;
    this.gigPicture = this.setGigPicture(gig.id);
    this.slides.lockSwipeToNext(false);
    this.slides.slideNext();
  }

  async setGigPicture(id) {
    try {
      const url = await this.gigService.getGigPicture(id);
      return url;
    } catch (error) {
      console.log(error);
    }
  }

  slideChanged() {
    if (this.slides.getActiveIndex() == 0) {
      this.slides.lockSwipeToNext(true);
    } else if (this.slides.getActiveIndex() == 1) {
      this.slides.lockSwipeToPrev(false);
    }
  }

  async applyToGig(gig) {
    try {
      await this.gigService.applyToGig(gig.id);
      this.dialogUtilities.showToast("Successfully applied to gig");
    } catch (e) {
      this.dialogUtilities.showToast(e);
    }
  }

  async chat(email: string) {
    try {
      const user = await this.preditorService.getPreditorByEmail(email);
      this.navCtrl.push("MessagesPage", { user }, { direction: "forward" });
    } catch (e) {
      this.dialogUtilities.showToast('Unable to chat with the creator.');
    }
  }

}