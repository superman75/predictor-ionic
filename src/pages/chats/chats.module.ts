import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatsPage } from './chats';
import { ChatFilterPipe } from './chatFilter.pipe';

@NgModule({
  declarations: [
    ChatsPage,
    ChatFilterPipe
  ],
  imports: [
    IonicPageModule.forChild(ChatsPage),
  ],
  exports: [
    ChatsPage
  ]
})
export class ChatsPageModule {}
