import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'chatFilter',
    pure: false
})
export class ChatFilterPipe implements PipeTransform {
    transform(predators: any[], filter: string): any {
        if (!predators || !filter) {
            return predators;
        }
        return predators.filter(x => x.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
    }
}
