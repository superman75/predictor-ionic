import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PreditorService } from '../../providers/preditor.service';
import { DialogUtilitiesProvider } from '../../providers/dialog-utilities';
import { ChatProvider } from '../../providers/chat.service';

@IonicPage()
@Component({
  selector: 'page-chats',
  templateUrl: 'chats.html',
  providers: [PreditorService]
})
export class ChatsPage {

  preditors: any = [];
  chats: any = {};

  constructor(private navCtrl: NavController,
    private preditorService: PreditorService,
    private dialogUtilities: DialogUtilitiesProvider,
    private chatProvider: ChatProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatsPage');
  }

  async ionViewWillEnter() {
    this.preditors = await this.getPreditors();
    this.setLastMessages();
  }

  async getPreditors() {
    try {
      return await this.preditorService.getPreditors();
    } catch (e) {
      this.dialogUtilities.showToast(e);
      console.log("Error while retrieving preditors", e);
    }
  }

  setLastMessages() {
    for (let user of this.preditors) {
      this.getLastChat(user);
    }
  }

  async getLastChat(user) {
    try {
      const chats$ = await this.chatProvider.getLastChat(user);
      chats$.valueChanges()
        .subscribe(chats => {
          this.chats[user.id] = chats.length ? chats.shift().message : 'No messages';
        });
    } catch (e) {
      return this.chats[user.id] = 'Error..';
    }
  }

  chatTo(preditor) {
    this.navCtrl.push("MessagesPage", { user: preditor }, { direction: "forward" });
  }

}
