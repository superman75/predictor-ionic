import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { DialogUtilitiesProvider } from '../../providers/dialog-utilities';
import { AuthProvider } from '../../providers/auth.service';
import { ProfileProvider } from '../../providers/profile.service';

@IonicPage()
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html'
})
export class SignInPage {

  user: any = {};

  constructor(private navCtrl: NavController,
    private dialogUtilities: DialogUtilitiesProvider,
    private auth: AuthProvider,
    private profileProvider: ProfileProvider) {

  }

  async login(provider = "local") {
    try {
      this.dialogUtilities.showLoading();
      if (provider == "local") {
        await this.auth.login(this.user.email, this.user.password);
      } else if (provider == "google") {
        await this.auth.loginViaGoogle();
      }
      else if (provider == "facebook") {
        await this.auth.loginViaFacebook();
      }
      const nextPage: any = await this.profileProvider.getStartupPage();
      this.navCtrl.setRoot(nextPage);
    } catch (e) {
      this.dialogUtilities.showToast(e);
    } finally {
      this.dialogUtilities.hideLoading();
    }
  }

  goToSignUp() {
    this.navCtrl.setRoot('SignUpPage');
  }

}