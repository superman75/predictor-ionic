import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-gigs',
  templateUrl: 'gigs.html'
})
export class GigsPage {

    items = [
        'Post a Gig',
        'Find a Gig',
        'My Gigs'
    ];

    constructor(private navCtrl: NavController) {

    }

    selectItem(item) {
        if(item == 'Find a Gig') {
            this.navCtrl.push('FindAGigPage');
        } else if(item == 'Post a Gig') {
            this.navCtrl.push('PostAGigPage');
        } else if(item == 'My Gigs') {
            this.navCtrl.push('MyGigsPage');
        }
    }

}