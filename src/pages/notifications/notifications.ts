import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { NotificationService } from '../../providers/notification.service';

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
  providers: [NotificationService]
})
export class NotificationsPage {

  notifications: any = [];

  constructor(private navCtrl: NavController, private notificationService: NotificationService) {

  }

  ionViewWillEnter() {
    this.getNotifications();
  }

  getNotifications() {
    this.notificationService.getNotifications().subscribe( data => {
      this.notifications = data['data'];
    })
  }

}