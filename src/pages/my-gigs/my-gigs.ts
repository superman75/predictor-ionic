import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides, Platform } from 'ionic-angular';
import { GigService } from '../../providers/gigs.service';
import { PreditorService } from '../../providers/preditor.service';
import { DialogUtilitiesProvider } from '../../providers/dialog-utilities';

@IonicPage()
@Component({
  selector: 'page-my-gigs',
  templateUrl: 'my-gigs.html',
  providers: [GigService, PreditorService]
})
export class MyGigsPage {

  @ViewChild(Slides) slides: Slides;
  canLeave: Boolean = true;
  gigs: any = [];
  selectedGig: any = {};
  gigPicture: any = null;
  talents: any = [];
  selectedTalent: any = {};

  constructor(private platform: Platform,
    private preditorService: PreditorService,
    private gigService: GigService,
    private dialogUtilities: DialogUtilitiesProvider,
    private navCtrl: NavController) {
    this.platform.backButton.subscribe(data => {
      if (this.slides.getActiveIndex() == 1 || this.slides.getActiveIndex() == 2
        || this.slides.getActiveIndex() == 3 || this.slides.getActiveIndex() == 4) {
        this.canLeave = false;
        this.slides.slidePrev();
        setTimeout(() => {
          this.canLeave = true;
        }, 300);
      }
    });
  }

  async ionViewWillEnter() {
    this.slides.enableKeyboardControl(false);
    this.slides.lockSwipeToNext(true);
    this.gigs = await this.getMyGigs();
  }

  ionViewCanLeave() {
    return this.canLeave;
  }

  async getMyGigs() {
    try {
      const gigs: any = await this.gigService.getMyGigs();
      return gigs;
    } catch (e) {
      this.dialogUtilities.showToast("There was an issue retrieving gigs");
    }
    return [];
  }

  async selectItem(gig) {
    this.selectedGig = gig;
    this.gigPicture = this.setGigPicture(gig.id);
    this.slides.lockSwipeToNext(false);
    this.slides.slideNext();
  }

  async setGigPicture(id) {
    try {
      const url = await this.gigService.getGigPicture(id);
      return url;
    } catch (error) {
      console.log(error);
    }
  }

  goToProfile(talent) {
    this.selectedTalent = talent;
    this.slides.lockSwipeToNext(false);
    this.slides.slideNext();
  }

  goToTalents() {
    if (!this.selectedGig) {
      return;
    }
    this.getTalents();
    this.slides.lockSwipeToNext(false);
    this.slides.slideNext();
  }

  async getTalents() {
    const preditors: any = await this.preditorService.getPreditors();
    const talents = preditors.filter(x => this.selectedGig.talents && this.selectedGig.talents.indexOf(x.emailAddress) !== -1);
    this.talents = talents;
  }

  slideChanged() {
    if (this.slides.getActiveIndex() == 0) {
      this.slides.lockSwipeToNext(true);
    } else if (this.slides.getActiveIndex() == 1) {
      this.slides.lockSwipeToPrev(false);
      this.slides.lockSwipeToNext(true);
    } else if (this.slides.getActiveIndex() == 2) {
      this.slides.lockSwipeToPrev(false);
      this.slides.lockSwipeToNext(true);
    } else if (this.slides.getActiveIndex() == 3) {
      this.slides.lockSwipeToPrev(false);
    }
  }

  chatTo(user) {
    this.navCtrl.push("MessagesPage", { user }, { direction: "forward" });
  }

  checkAvailability() {
    if (!this.selectedTalent) {
      return;
    }
    
    this.navCtrl.push("CalendarPage", { user:  this.selectedTalent});
  }
}