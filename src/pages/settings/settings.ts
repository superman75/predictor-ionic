import { Component } from '@angular/core';
import { IonicPage, NavController, Events } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth.service';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  items: any = [
    'Notifications',
    'Calendar',
    'Invitations',
    'Privacy Policy',
    'Terms of Service',
    'Clear Cache',
    'Blocked Users',
    'Logout'
  ];

  constructor(private navCtrl: NavController,
    private auth: AuthProvider,
    private events: Events) {

  }

  async itemSelected(item) {
    if (item == 'Calendar') {
      this.navCtrl.push('CalendarPage');
    } else if (item == 'Notifications') {
      this.navCtrl.push('NotificationsPage');
    } else if (item == 'Logout') {
      this.events.publish("user:logout");
    } else if(item == 'Privacy Policy') {
      this.navCtrl.push('PrivacyPolicyPage');
    } else if(item == 'Terms of Service') {
      this.navCtrl.push('TermsConditionsPage');
    }
  }
}