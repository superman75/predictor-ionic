import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides, Platform, ModalController } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';
import { NetworkService } from '../../providers/network.service';
import { CalendarModalOptions, CalendarModal, CalendarResult } from 'ion2-calendar';
import { Gig, GigPositions } from '../../models/gig';
import { GigService } from '../../providers/gigs.service';
import { DialogUtilitiesProvider } from '../../providers/dialog-utilities';

@IonicPage()
@Component({
  selector: 'page-post-a-gig',
  templateUrl: 'post-a-gig.html',
  providers: [NetworkService, GigService]
})
export class PostAGigPage {

  @ViewChild(Slides) slides: Slides;
  selectedDate: any
  isNetwork: Boolean = false;
  canLeave: Boolean = true;
  networks: any = [];
  selectedNetwork: any = {};
  gig: Gig = new Gig();
  positions: any = GigPositions;
  @ViewChild("imageFile") imageFile: any;

  constructor(private datePicker: DatePicker,
    private networkService: NetworkService,
    private platform: Platform,
    private modalCtrl: ModalController,
    private gigService: GigService,
    private dialogUtilities: DialogUtilitiesProvider,
    private navCtrl: NavController) {
    this.platform.backButton.subscribe(data => {
      if (this.slides.getActiveIndex() == 1 || this.slides.getActiveIndex() == 2) {
        this.canLeave = false;
        this.slides.slidePrev();
        setTimeout(() => {
          this.canLeave = true;
        }, 300);
      }
    });
  }

  ionViewWillEnter() {
    this.slides.enableKeyboardControl(false);
    this.slides.lockSwipeToNext(true);
  }

  ionViewCanLeave() {
    return this.canLeave;
  }

  async getNetworks(position) {
    this.networks = await this.networkService.getNetworks(position);
  }

  selectDate(event) {
    event.stopPropagation();
    const options: CalendarModalOptions = {
      title: "GIG DATES",
      pickMode: "range"
    };
    let calendar = this.modalCtrl.create(CalendarModal, {
      options: options
    });

    calendar.present();

    calendar.onDidDismiss((date: { from: CalendarResult, to: CalendarResult }, type: string) => {
      this.gig.startDate = date.from.time as any;
      this.gig.endDate = date.to.time as any;
    });
  }

  goToNetworks() {
    this.getNetworks(this.gig.position);
    this.slides.lockSwipeToNext(false);
    this.slides.slideNext();
    this.isNetwork = true;
  }

  goToProfile(network) {
    this.selectedNetwork = network;
    this.slides.lockSwipeToNext(false);
    this.slides.slideNext();
  }

  formatDate(date) {
    let monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + ' ' + monthNames[monthIndex] + ' ' + year;
  }

  slideChanged() {
    if (this.slides.getActiveIndex() == 0) {
      this.slides.lockSwipeToNext(true);
      this.isNetwork = false;
    } else if (this.slides.getActiveIndex() == 1) {
      this.slides.lockSwipeToPrev(false);
      this.slides.lockSwipeToNext(true);
      this.isNetwork = true;
    } else if (this.slides.getActiveIndex() == 2) {
      this.slides.lockSwipeToPrev(false);
      this.isNetwork = false;
    }
  }

  pickImage() {
    this.imageFile.nativeElement.click();
  }

  async submit() {
    try {
      this.dialogUtilities.showLoading();
      const image = this.imageFile.nativeElement.files.length > 0 ? this.imageFile.nativeElement.files[0] : null;
      const gig: any = await this.gigService.addGig(this.gig);
      if (image) {
        await this.gigService.updateGigPicture(gig.id, image);
      }
      this.goToNetworks();
    } catch (e) {
      this.dialogUtilities.showToast(e);
    } finally {
      this.dialogUtilities.hideLoading();
    }
  }

  chatTo(user) {
    this.navCtrl.push("MessagesPage", { user }, { direction: "forward" });
  }

  checkAvailability() {
    if (!this.selectedNetwork) {
      return;
    }     
    
    this.navCtrl.push("CalendarPage", { user:  this.selectedNetwork});
  }

}