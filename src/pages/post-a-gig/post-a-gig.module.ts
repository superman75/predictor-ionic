import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { PostAGigPage } from './post-a-gig';

@NgModule({
  declarations: [
    PostAGigPage,
  ],
  imports: [
    IonicPageModule.forChild(PostAGigPage)
  ],
  exports: [
    PostAGigPage
  ]
})
export class PostAGigPageModule { }
