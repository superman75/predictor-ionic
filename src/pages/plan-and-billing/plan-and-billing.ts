import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides, Platform } from 'ionic-angular';
import { ProfileProvider } from '../../providers/profile.service';
import { DialogUtilitiesProvider } from '../../providers/dialog-utilities';
import { Billing } from '../../models/billing';
import { Plan } from '../../models/plan';
import { AuthProvider } from '../../providers/auth.service';

@IonicPage()
@Component({
    selector: 'page-plan-and-billing',
    templateUrl: 'plan-and-billing.html'
})
export class PlanAndBillingPage {

    @ViewChild(Slides) slides: Slides;
    isMembership: Boolean = false;
    isBilling: Boolean = false;
    canLeave: Boolean = true;
    days: number;
    membership: string;
    billing: Billing = new Billing();

    constructor(private navCtrl: NavController,
        private platform: Platform,
        private profileProvider: ProfileProvider,
        private dialogUtilities: DialogUtilitiesProvider,
        private auth: AuthProvider) {
        this.platform.backButton.subscribe(data => {
            if (this.slides.getActiveIndex() == 1 || this.slides.getActiveIndex() == 2) {
                this.slides.slidePrev();
            }
        });
    }

    // Add this to all the pages that needs checking if logged in
    async ionViewCanEnter() {
        return await this.auth.isLoggedIn();
    }

    ionViewCanLeave() {
        return this.canLeave;
    }

    async ionViewWillEnter() {
        this.slides.enableKeyboardControl(false);
        this.slides.lockSwipeToNext(true);
        this.billing = await this.getBillingInfo();
    }

    async getBillingInfo() {
        try {
            const profile: any = await this.profileProvider.getProfile();
            return profile.billing || new Billing();
        } catch (e) {
            this.dialogUtilities.showToast("There was an issue retrieving billing info");
        }
        return new Billing();
    }

    async submitBilling() {
        try {
            await this.profileProvider.updatePlan(new Plan({ days: this.days, membership: this.membership }));
            await this.profileProvider.updateBilling(this.billing);
            this.canLeave = true;
            const nextPage: any = await this.profileProvider.getStartupPage();
            this.navCtrl.setRoot(nextPage);
        } catch (e) {
            this.dialogUtilities.showToast(e);
        }
    }

    async selectDays(days) {
        this.slides.lockSwipeToNext(false);
        this.slides.slideNext();
        this.isMembership = true;
        this.days = days;
    }

    selectMembership(membership) {
        this.slides.lockSwipeToNext(false);
        this.slides.slideNext();
        this.isBilling = true;
        this.membership = membership;
    }

    slideChanged() {
        if (this.slides.getActiveIndex() == 0) {
            this.canLeave = true;
        } else if (this.slides.getActiveIndex() == 1) {
            this.slides.lockSwipeToNext(true);
            this.slides.lockSwipeToPrev(false);
            this.isBilling = false;
            this.isMembership = true;
            this.canLeave = false;
        } else if (this.slides.getActiveIndex() == 2) {
            this.slides.lockSwipeToNext(true);
            this.slides.lockSwipeToPrev(false);
            this.isBilling = true;
            this.isMembership = false;
            this.canLeave = false;
        }
    }

}