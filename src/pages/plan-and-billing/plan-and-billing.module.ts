import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { PlanAndBillingPage } from './plan-and-billing';

@NgModule({
  declarations: [
    PlanAndBillingPage,
  ],
  imports: [
    IonicPageModule.forChild(PlanAndBillingPage)
  ],
  exports: [
    PlanAndBillingPage
  ]
})
export class PlanAndBillingPageModule { }
