import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth.service';
import { User } from '../../models/user';
import { DialogUtilitiesProvider } from '../../providers/dialog-utilities';

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html'
})
export class SignUpPage {

  user: User = new User();
  password: string = "";
  @ViewChild("imageFile") imageFile: any;
  @ViewChild("resumeFile") resumeFile: any;

  constructor(private navCtrl: NavController,
    private auth: AuthProvider,
    private dialogUtilities: DialogUtilitiesProvider) {

  }

  goToSignIn() {
    this.navCtrl.setRoot('SignInPage');
  }

  async signUp() {
    try {
      this.dialogUtilities.showLoading();
      const image = this.imageFile.nativeElement.files.length > 0 ? this.imageFile.nativeElement.files[0] : null;
      const resume = this.resumeFile.nativeElement.files.length > 0 ? this.resumeFile.nativeElement.files[0] : null;
      await this.auth.register(this.user, this.password, resume, image);
      this.dialogUtilities.showToast("Registration successful");
      this.navCtrl.setRoot('SignInPage');
    } catch (e) {
      this.dialogUtilities.showToast(e);
    } finally {
      this.dialogUtilities.hideLoading();
    }
  }

  pickImage() {
    this.imageFile.nativeElement.click();
  }

  pickResume() {
    this.resumeFile.nativeElement.click();
  }

  openBrowser(url) {
    this.dialogUtilities.openInAppBrowser(url);
  }

}