import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChatProvider } from '../../providers/chat.service';
import { DialogUtilitiesProvider } from '../../providers/dialog-utilities';
import { ProfileProvider } from '../../providers/profile.service';
import { Chat } from '../../models/chat';

@IonicPage()
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html'
})
export class MessagesPage {

  chats: any = [];
  message: string = "";
  otherUser: any;
  ownPicture: any = "";
  @ViewChild("content") content: any;
  scrollingSpeed = 300;

  constructor(private navParams: NavParams,
    private chatProvider: ChatProvider,
    private dialogUtilities: DialogUtilitiesProvider,
    private profileProvider: ProfileProvider) {

  }

  async ionViewWillEnter() {
    this.otherUser = this.navParams.get("user");
    console.log('otherUser')
    await this.getChats(this.otherUser);
    this.ownPicture = await this.profileProvider.getProfilePicture();
    this.content.scrollToBottom(this.scrollingSpeed);
  }

  async getChats(user) {
    try {
      const chats$ = await this.chatProvider.getChats(user);
      chats$.valueChanges().subscribe(chats => {
        this.chats = chats.map(chat => new Chat({ ...chat }));
      });
    } catch (e) {
      this.dialogUtilities.showToast(e);
      console.log("Error while retrieving messages", e);
    }
  }

  async sendMessage() {
    this.chatProvider.addChat(this.message, this.otherUser);
    this.message = ""
    setTimeout(() => {
      this.content.scrollToBottom(this.scrollingSpeed);
    }, 100);
  }

  isChatFromOther(chat) {
    return chat.sender == this.otherUser.emailAddress;
  }

}