import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { PreditorService } from '../../providers/preditor.service';
import { DialogUtilitiesProvider } from '../../providers/dialog-utilities';
import { ChatProvider } from '../../providers/chat.service';

@IonicPage()
@Component({
  selector: 'page-preditor',
  templateUrl: 'preditor.html',
  providers: [PreditorService]
})
export class PreditorPage {

  preditors: any = [];

  constructor(private navCtrl: NavController,
    private preditorService: PreditorService,
    private dialogUtilities: DialogUtilitiesProvider,
    private chatProvider: ChatProvider) {

  }

  async ionViewWillEnter() {
    this.preditors = await this.getPreditors();
  }

  async getPreditors() {
    try {
      return await this.preditorService.getPreditors();
    } catch(e) {
      this.dialogUtilities.showToast(e);
      console.log("Error while retrieving preditors", e);
    }
  }

  userSearch() {
    this.navCtrl.push('UserSearchPage');
  }

  chatTo(preditor) {
    // this.chatProvider.createSingleChatRoom(preditor);
  }

}