import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { PreditorPage } from './preditor';

@NgModule({
  declarations: [
    PreditorPage,
  ],
  imports: [
    IonicPageModule.forChild(PreditorPage)
  ],
  exports: [
    PreditorPage
  ]
})
export class PreditorPageModule { }
