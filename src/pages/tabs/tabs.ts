import { Component, ViewChild } from '@angular/core';
import { NavController, IonicPage, Tabs } from 'ionic-angular';
import { GigsPage, ProfilePage, SettingsPage, PreditorPage, ChatsPage} from '../';
import { Tab } from 'ionic-angular/navigation/nav-interfaces';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  profile: any = ProfilePage;
  gigs: any = GigsPage;
  chats: any = ChatsPage;
  settings: any = SettingsPage;
  preditor: any = PreditorPage;

  @ViewChild('tabs') tabRef: Tabs;

  constructor(public navCtrl: NavController) {
  }

  ionViewWillEnter() {
  }

  listenTabChange(tab: Tab) {
    console.log('index', tab.index);
    let tabbar = document.getElementsByClassName('tabbar')[0];
    let tabs = tabbar.getElementsByTagName('a');
    if(tab.index == 1) {
      tabs[1].removeChild(tabs[1].childNodes[1]);
      let img = this.createIconImage('assets/imgs/blackgigs.png');
      tabs[1].insertBefore(img, tabs[1].childNodes[1]);

      tabs[4].removeChild(tabs[4].childNodes[1]);
      let img2 = this.createIconImage('assets/imgs/preditor.png');
      tabs[4].insertBefore(img2, tabs[4].childNodes[1]);
    } else if(tab.index == 4) {
      tabs[4].removeChild(tabs[4].childNodes[1]);
      let img = this.createIconImage('assets/imgs/blackpreditor.png');
      tabs[4].insertBefore(img, tabs[4].childNodes[1]);

      tabs[1].removeChild(tabs[1].childNodes[1]);
      let img1 = this.createIconImage('assets/imgs/gigs.png');
      tabs[1].insertBefore(img1, tabs[1].childNodes[1]);
    } else {
      let tabbar = document.getElementsByClassName('tabbar')[0];
      let tabs = tabbar.getElementsByTagName('a');

      tabs[1].removeChild(tabs[1].childNodes[1]);
      tabs[4].removeChild(tabs[4].childNodes[1]);
      let img1 = this.createIconImage('assets/imgs/gigs.png');
      let img2 = this.createIconImage('assets/imgs/preditor.png');
      tabs[1].insertBefore(img1, tabs[1].childNodes[1]);
      tabs[4].insertBefore(img2, tabs[4].childNodes[1]);
    }
  }
 
  private createIconImage(iconUri: string){
    let img = document.createElement("img");
    img.setAttribute("class", "tab-icon-custom tab-button-icon icon icon-md ion-md-bookmarks");
    img.setAttribute("style", "width:20px; height: 20px; margin-bottom: 4px; display: block !important");
    img.setAttribute("src", iconUri);
    return img;
  }
}
