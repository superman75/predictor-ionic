import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, APP_INITIALIZER } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { DatePicker } from '@ionic-native/date-picker';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { AuthProvider } from '../providers/auth.service';
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook } from '@ionic-native/facebook';
// import { Push } from '@ionic-native/push';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
import * as firebase from 'firebase/app';
import { environment } from '../environments/environment';
import { DialogUtilitiesProvider } from '../providers/dialog-utilities';
import { ProfileProvider } from '../providers/profile.service';
import { CalendarModal, CalendarModule } from 'ion2-calendar';
import { UtilitiesProvider } from '../providers/utilities';
import { ChatProvider } from '../providers/chat.service';

// Initialize firebase
firebase.initializeApp(environment.firebase);

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      mode: "md"
    }),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireFunctionsModule,
    CalendarModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CalendarModal
  ],
  providers: [
    StatusBar,
    DatePicker,
    SplashScreen,
    InAppBrowser,
    GooglePlus,
    Facebook,
    // Push,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthProvider,
    DialogUtilitiesProvider,
    ProfileProvider,
    {
      provide: APP_INITIALIZER,
      useFactory: (pp: ProfileProvider) => () => pp.init(),
      deps: [ProfileProvider],
      multi: true
    },
    UtilitiesProvider,
    ChatProvider
  ]
})
export class AppModule { }
