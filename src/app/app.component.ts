import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { LandingPage, TabsPage } from '../pages';
import { AuthProvider } from '../providers/auth.service';
import { ProfileProvider } from '../providers/profile.service';
import { DialogUtilitiesProvider } from '../providers/dialog-utilities';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = LandingPage;
  @ViewChild(Nav) nav: Nav;

  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private auth: AuthProvider,
    private profileProvider: ProfileProvider,
    private dialogUtilities: DialogUtilitiesProvider,
    private event: Events) {
    platform.ready().then(async () => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      // subscribe to events
      this.event.subscribe("user:logout", this.logout.bind(this));

      this.dialogUtilities.showLoading();
      await this.checkAuthentication();
      this.dialogUtilities.hideLoading();
      // this.pushSetup();
    });
  }

  // pushSetup() {
  //   const options: PushOptions = {
  //     android: {
  //       senderID: '835863859224'
  //     },
  //     ios: {
  //         alert: 'true',
  //         badge: true,
  //         sound: 'false'
  //     },
  //     windows: {},
  //     browser: {
  //         pushServiceURL: 'http://push.api.phonegap.com/v1/push'
  //     }
  //   };
    
  //   const pushObject: PushObject = this.push.init(options);
    
    
  //   pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));
    
  //   pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));
    
  //   pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  // }

  async checkAuthentication() {
    try {
      // Authentication
      console.log("trying to authenticate");
      await this.auth.isLoggedIn();
      const startupPage = await this.profileProvider.getStartupPage();
      console.log("user authenticated", startupPage);
      if (startupPage != null) {
        this.nav.setRoot(startupPage);
      }
    } catch (e) {
      console.log("user not authenticated");
      this.nav.setRoot("LandingPage");
    }
  }

  // root events (e.g. Logout)
  async logout() {
    await this.auth.logout();
    this.nav.setRoot("SignInPage");
  }
}

